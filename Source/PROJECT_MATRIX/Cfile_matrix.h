#pragma once

//TOOL FOR CHECKING MEMORY LEAKS
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifdef _DEBUG
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif

#include<fstream>
#include<iostream>
#include"Cexception.h"
#include<time.h>
using namespace std;

#define FILexceptionTheFileIsNotReadeable 0
#define FILexceptionStructureOfTheFileIncorrect 1
#define FILexceptionNumberOfRowsOrAndColumnsAreNotLogical 2
#define FILexceptionValueOfTheMatrixIsNotLogical 3


class Cfile_matrix
{
	protected :

		ifstream file;
		char* sFILname;
		char* sFILtype;

		int iFILnbOfRows;
		int iFILnbOfColumns;

		double* pFILmatrixContent;
		
	public:

		/**
		 * @param[in] nothing
		 * @cond nothing
		 * @return nothing
		 * @brief The current object is initialized with the default values for the attributes : sFILname = nullptr, sFILtype = nullptr, iFILnbOfRows = 0, iFILnbOfColumns = 0, pFILmatrixContent = nullptr.
		 */
		Cfile_matrix();

		/**
		 * @param[in] a pointer on characters
		 * @cond nothing
		 * @return nothing
		 * @brief The current object's name is initialized by the value of the parameter.
		 */
		Cfile_matrix(char* sFILnamePARAM);


		/**
		 * @param[in] nothing
		 * @cond nothing
		 * @return nothing
		 * @brief deletes the allocated attributes of the current Cfile_matrix and destroys the Cfile_matrix.
		 */
		~Cfile_matrix();


		/**
		 * @param[in] nothing
		 * @cond nothing
		 * @return the reference of the current Cfile_matrix
		 * @brief  replaces the values of the attributes of the current Cfile_matrix by the values of the Cfile_matrix in parameter.
		 */
		Cfile_matrix& operator=(const Cfile_matrix& file);
		

		/**
		 * @param[in] nothing
		 * @cond a Cfile_matrix already initialized with the name of the file
		 * @return a boolean
		 * @brief open the file whose name is the value of the attribute sFILNAME, then gets the type of the Cmatrix wanted, the number of rows and columns and the values in the Cmatrix
		 */
		bool FILparsing();


		/**
		 * @param[in] a pointer on characters and a character
		 * @cond a not null pointer
		 * @return a boolean to indicate if the character is in the character string or not.
		 * @brief searches for the character c in the string param. If param contains c, returns true, else returns false.
		 */
		bool FILfind_occurence(char* param, char c)
		{
			int i = 0;
			while (param[i] != '\0')
			{
				if (param[i] == c)
				{
					return true;
				}
				i++;
			}
			return false;
		}


		/**
		 * @param[in] a const pointer on characters and a character
		 * @cond a not null pointer
		 * @return a boolean to indicate if string is a correct number or not.
		 * @brief searches for a number in the string param.If param contains a sign or a dot, the number remains correct, but with multiple dots or signs or just one comma, it is not correct.
		 */
		static bool FILisItANumber(const char* param)
		{
			bool hasAlreadyAPoint = false;
			bool hasAlreadyASign = false;

			int iLoop=0;
			while (param[iLoop] != '\0')
			{
				if ( (param[0] == '+' || param[0] == '-') && hasAlreadyASign == false) //Okay no problem...
				{
					hasAlreadyASign = true;
					iLoop++;
				}

				else if (param[iLoop] == '+' || param[iLoop] == '-' || param[iLoop] == ',' || (param[iLoop] == '.' && hasAlreadyAPoint == true) || (param[iLoop]>'9' && param[iLoop]<'0'))
				{
					return false;
				}

				else if (param[iLoop] == '.') //If we want to type 3,2 then we must type 3.2 !!!!!!
				{
					hasAlreadyAPoint = true;
					iLoop++;
				}

				else
				{
					iLoop++;
				}
			}
			return true;
		}


		/**
		 * @param[in] nothing
		 * @cond nothing
		 * @return the name of the file of the current Cfile_matrix.
		 * @brief returns the name of the Cfile_matrix.
		 */
		char* FILget_file_name()
		{
			return sFILname;
		}


		/**
		 * @param[in] a pointer on characters
		 * @cond nothing
		 * @return nothing
		 * @brief sets the name of the current Cfile_matrix with the parameter.
		 */
		void FILset_file_name(char* param)
		{
			if (sFILname != nullptr)
			{
				delete[] sFILname;
			}

			sFILname = new char[strlen(param)+1];
			strcpy_s(sFILname, strlen(param)+1, param);		
		}


		/**
		 * @param[in] a pointer on characters
		 * @cond nothing
		 * @return the type of the Cmatrix of the current Cfile_matrix
		 * @brief returns the type of the Cmatrix of the current Cfile_matrix.
		 */
		char* FILget_file_type()
		{
			return sFILtype;
		}


		/**
		 * @param[in] nothing
		 * @cond nothing
		 * @return the type of the Cmatrix of the current Cfile_matrix
		 * @brief returns the number of rows of the Cmatrix of the current Cfile_matrix.
		 */
		int FILget_file_nb_rows()
		{
			return iFILnbOfRows;
		}


		/**
		 * @param[in] nothing
		 * @cond nothing
		 * @return the number of columns of the Cmatrix of the current Cfile_matrix
		 * @brief returns the number of columns of the Cmatrix of the current Cfile_matrix.
		 */
		int FILget_file_nb_columns()
		{
			return iFILnbOfColumns;
		}

		/**
		 * @param[in] nothing
		 * @cond nothing
		 * @return a pointer to an array which contain the element of the matrix
		 * @brief returns the array of element of the matrix
		 */
		double* FILget_file_matrix_content()
		{
			return pFILmatrixContent;
		}
};

