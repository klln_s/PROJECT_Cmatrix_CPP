#include"Cfile_matrix.h"
#include<iostream>

/**
 * @param[in] nothing
 * @cond nothing
 * @return nothing
 * @brief The current object is initialized with the default values for the attributes : sFILname = nullptr, sFILtype = nullptr, iFILnbOfRows = 0, iFILnbOfColumns = 0, pFILmatrixContent = nullptr.
 */
Cfile_matrix::Cfile_matrix()
{
	sFILname = nullptr;
	sFILtype = nullptr;
	iFILnbOfRows = 0;
	iFILnbOfColumns = 0;
	pFILmatrixContent = nullptr;
}


/**
 * @param[in] a pointer on characters
 * @cond nothing
 * @return nothing
 * @brief The current object's name is initialized by the value of the parameter.
 */
Cfile_matrix::Cfile_matrix(char* sFILnamePARAM)
{
	if (sFILname != nullptr)
	{
		delete[] sFILname;
		sFILname = nullptr;
	}
	sFILname = new char[strlen(sFILnamePARAM)+1];
	strcpy_s(sFILname, strlen(sFILnamePARAM)+1, sFILnamePARAM);
}


/**
 * @param[in] nothing
 * @cond nothing
 * @return the reference of the current Cfile_matrix
 * @brief  replaces the values of the attributes of the current Cfile_matrix by the values of the Cfile_matrix in parameter.
 */
Cfile_matrix& Cfile_matrix::operator=(const Cfile_matrix& fileParam)
{

	if (this != &fileParam) //we check if don't try to make A = A
	{
		iFILnbOfColumns = fileParam.iFILnbOfColumns;
		iFILnbOfRows = fileParam.iFILnbOfRows;

		if (sFILname != nullptr)
		{
			delete[] sFILname;
		}

		if (sFILtype != nullptr)
		{
			delete[] sFILtype;
		}

		if (pFILmatrixContent != nullptr)
		{
			delete[] pFILmatrixContent;
		}

		if (fileParam.sFILname != nullptr)
		{
			sFILname = new char[strlen(fileParam.sFILname)+1];
			strcpy_s(sFILname, strlen(fileParam.sFILname)+1, fileParam.sFILname);
		}

		if (fileParam.sFILtype != nullptr)
		{
			sFILtype = new char[strlen(fileParam.sFILtype)+1];
			strcpy_s(sFILtype, strlen(fileParam.sFILtype)+1, fileParam.sFILtype);
		}

		if (fileParam.pFILmatrixContent != nullptr && (iFILnbOfColumns > 0 && iFILnbOfRows > 0))
		{
			pFILmatrixContent = new double[iFILnbOfColumns*iFILnbOfRows];
			memcpy(pFILmatrixContent, fileParam.pFILmatrixContent, iFILnbOfColumns*iFILnbOfRows*sizeof(double));
		}

	}
	return *this;
}


/**
 * @param[in] nothing
 * @cond nothing
 * @return nothing
 * @brief deletes the allocated attributes of the current Cfile_matrix and destroys the Cfile_matrix.
 */
Cfile_matrix::~Cfile_matrix()
{
	if (sFILname != nullptr)
	{
		delete[] sFILname;
		sFILname = nullptr;
	}
	if (sFILtype != nullptr)
	{
		delete[] sFILtype;
		sFILtype = nullptr;
	}

	if (pFILmatrixContent != nullptr)
	{
		delete[] pFILmatrixContent;
		pFILmatrixContent = nullptr;
	}
}

/**
 * @param[in] nothing
 * @cond a Cfile_matrix already initialized with the name of the file
 * @return a boolean 
 * @brief open the file whose name is the value of the attribute sFILNAME, then gets the type of the Cmatrix wanted, the number of rows and columns and the values in the Cmatrix
 */
bool Cfile_matrix::FILparsing() //to parse the files and get all the informations we need
{
	char sBuffer0[200];

	file.open(sFILname);

	if (file.is_open() == false)
	{
		Cexception EXCobject;
		EXCobject.EXCset_value(FILexceptionTheFileIsNotReadeable);
		throw(EXCobject);
	}

	time_t tStart, tEnd;
	time(&tStart);
	for (int iLoop4FirstLines = 0; iLoop4FirstLines < 4; iLoop4FirstLines++)
	{
		file.getline(sBuffer0, 200);

		if (strcmp(sBuffer0, "") == 0) //if the line read is empty...
		{
			iLoop4FirstLines--;

			time(&tEnd);
			double tTimeTaken = double(tEnd - tStart)/double(CLOCKS_PER_SEC);

			if (tTimeTaken > 0.0016) //If after 0.0016s, we still can't read anything, we consider that the file is simply empty and we don't read it.
			{
				Cexception EXCobject;
				EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
				throw(EXCobject);
			}
		}

		else if (iLoop4FirstLines == 0) //Line number 1
		{
			time(&tStart); //restart the time
			char sSeparator[] = "= <>";
			char* sToken1 = nullptr;
			char* sToken2 = nullptr;
			char* sNext_token1 = nullptr;

			sToken1 = strtok_s(sBuffer0, sSeparator, &sNext_token1);

			if (sToken1 == nullptr)
			{
				Cexception EXCobject;
				EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
				throw(EXCobject);
			}

			char* sBuffer1 = _strdup(sToken1);

			if (strcmp(sBuffer1, "TypeMatrice") != 0)
			{
				cout << "	>This : " << sBuffer1 << " has generated an exception " << endl;
				Cexception EXCobject;
				EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
				throw(EXCobject);
			}

			if (sToken1 != nullptr)
			{
				sToken2 = strtok_s(nullptr, sSeparator, &sNext_token1);
				if (sToken2 == nullptr)
				{
					Cexception EXCobject;
					EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
					throw(EXCobject);
				}
				else
				{
					char* sBuffer2 = _strdup(sToken2);
					sFILtype = new char[strlen(sBuffer2) + 1];
					strcpy_s(sFILtype, strlen(sBuffer2) + 1, sBuffer2);
					delete[] sBuffer2;
				}

			}
			delete[] sBuffer1;
		}


		else if (iLoop4FirstLines == 1) //Line number 2
		{
			time(&tStart); //restart the time
			char sSeparator[] = "= <>";
			char* sToken1 = nullptr;
			char* sToken2 = nullptr;
			char* sNext_token1 = nullptr;

			sToken1 = strtok_s(sBuffer0, sSeparator, &sNext_token1);

			if (sToken1 == nullptr)
			{
				Cexception EXCobject;
				EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
				throw(EXCobject);
			}
			char* sBuffer1 = _strdup(sToken1);

			if (strcmp(sBuffer1, "NBLignes") != 0)
			{
				cout << "	>This : " << sBuffer1 << " has generated an exception " << endl;
				Cexception EXCobject;
				EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
				delete[] sBuffer1;
				throw(EXCobject);
			}

			if (sToken1 != nullptr)
			{
				sToken2 = strtok_s(nullptr, sSeparator, &sNext_token1);
				if (sToken2 == nullptr)
				{
					Cexception EXCobject;
					EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
					delete[] sBuffer1;
					throw(EXCobject);
				}
				else if (FILfind_occurence(sToken2, '-') == true || FILfind_occurence(sToken2, ',') == true || FILfind_occurence(sToken2, '.') == true || FILfind_occurence(sToken2, '+') == true) //just to be sure that's it's a UNSIGNED INTEGER VALUE with no + !
				{
					cout << "	>This : " << sToken2 << " has generated an exception " << endl;
					Cexception EXCobject;
					EXCobject.EXCset_value(FILexceptionNumberOfRowsOrAndColumnsAreNotLogical);
					delete[] sBuffer1;
					throw(EXCobject);
				}
				else //we're sure that we have some integers...
				{
					char* sBuffer2 = _strdup(sToken2);

					if (FILisItANumber(sBuffer2) == true)
					{
						iFILnbOfRows = atoi(sBuffer2); //we convert the char* into an integer value
						delete[] sBuffer2;
					}
					else
					{
						cout << "	>This : " << sToken2 << " has generated an exception " << endl;
						Cexception EXCobject;
						EXCobject.EXCset_value(FILexceptionNumberOfRowsOrAndColumnsAreNotLogical);
						delete[] sBuffer1;
						delete[] sBuffer2;
						throw(EXCobject);
					}
				}
			}

			delete[] sBuffer1;
		}

		else if (iLoop4FirstLines == 2) //Line number 3
		{
			time(&tStart); //restart the time

			char sSeparator[] = "= <>";
			char* sToken1 = nullptr;
			char* sToken2 = nullptr;
			char* sNext_token1 = nullptr;

			sToken1 = strtok_s(sBuffer0, sSeparator, &sNext_token1);

			if (sToken1 == nullptr)
			{
				Cexception EXCobject;
				EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
				throw(EXCobject);
			}
			char* sBuffer1 = _strdup(sToken1);

			if (strcmp(sBuffer1, "NBColonnes") != 0)
			{
				cout << "	>This : " << sBuffer1 << " has generated an exception " << endl;
				Cexception EXCobject;
				EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
				delete[] sBuffer1;
				throw(EXCobject);
			}

			if (sToken1 != nullptr)
			{
				sToken2 = strtok_s(nullptr, sSeparator, &sNext_token1);
				if (sToken2 == nullptr)
				{
					Cexception EXCobject;
					EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
					delete[] sBuffer1;
					throw(EXCobject);
				}
				else if (FILfind_occurence(sToken2, '-') == true || FILfind_occurence(sToken2, ',') == true || FILfind_occurence(sToken2, '.') == true || FILfind_occurence(sToken2, '+') == true) //just to be sure that's it's a UNSIGNED INTEGER VALUE with no + !
				{
					cout << "	>This : " << sToken2 << " has generated an exception " << endl;
					Cexception EXCobject;
					EXCobject.EXCset_value(FILexceptionNumberOfRowsOrAndColumnsAreNotLogical);
					delete[] sBuffer1;
					throw(EXCobject);
				}
				else //we're sure that we'll have some integers...
				{
					char* sBuffer2 = _strdup(sToken2);

					if (FILisItANumber(sBuffer2) == true) //just to check if it's a number
					{
						iFILnbOfColumns = atoi(sBuffer2); //we convert the char* into an integer value
						delete[] sBuffer2;
					}
					else
					{
						cout << "	>This : " << sToken2 << " has generated an exception " << endl;
						Cexception EXCobject;
						EXCobject.EXCset_value(FILexceptionNumberOfRowsOrAndColumnsAreNotLogical);
						delete[] sBuffer1;
						delete[] sBuffer2;
						throw(EXCobject);
					}
				}
			}

			delete[] sBuffer1;
		}


		else if (iLoop4FirstLines == 3) //Line number 4
		{
			time(&tStart);
			char sSeparator[] = "= <>";
			char* sToken1 = nullptr;
			char* sToken2 = nullptr;
			char* sNext_token1 = nullptr;

			sToken1 = strtok_s(sBuffer0, sSeparator, &sNext_token1);

			if (sToken1 == nullptr)
			{
				Cexception EXCobject;
				EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
				throw(EXCobject);
			}
			char* sBuffer1 = _strdup(sToken1);

			if (strcmp(sBuffer1, "Matrice") != 0)
			{
				cout << "	>This : " << sBuffer1 << " has generated an exception " << endl;
				Cexception EXCobject;
				EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
				delete[] sBuffer1;
				throw(EXCobject);
			}

			if (sToken1 != nullptr)
			{
				sToken2 = strtok_s(nullptr, sSeparator, &sNext_token1);
				if (sToken2 == nullptr || strcmp(sToken2, "[") != 0 || FILfind_occurence(sToken2, '-') == true || FILfind_occurence(sToken2, ',') == true || FILfind_occurence(sToken2, '.') == true)
				{
					Cexception EXCobject;
					EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
					delete[] sBuffer1;
					throw(EXCobject);
				}
				else
				{
					char* sBuffer2 = _strdup(sToken2);
					delete[] sBuffer2;
				}
			}

			delete[] sBuffer1;
		}
	}

	if (iFILnbOfColumns > 0 && iFILnbOfRows > 0) //if 0 row and 0 column then... we don't read nothing
	{
		time(&tStart); //restart
		pFILmatrixContent = new double[iFILnbOfRows*iFILnbOfColumns];

		for (int iLoopFILnbOfRows = 0; iLoopFILnbOfRows < iFILnbOfRows; iLoopFILnbOfRows++)
		{
			file.getline(sBuffer0, 200);

			if (strcmp(sBuffer0, "") == 0) //if the line read is empty...
			{
				iLoopFILnbOfRows--;

				time(&tEnd);
				double tTimeTaken = double(tEnd - tStart) / double(CLOCKS_PER_SEC);

				if (tTimeTaken > 0.0008) //If after 0.0008s, we still can't read anything, we consider that the file is simply empty and we don't read it.
				{
					Cexception EXCobject;
					EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
					throw(EXCobject);
				}
			}
			else
			{
				char sSeparator[] = " <>";
				char* sToken1 = nullptr;
				char* sToken2 = nullptr;
				char* sNext_token1 = nullptr;

				sToken1 = strtok_s(sBuffer0, sSeparator, &sNext_token1);

				if (sToken1 == nullptr)
				{
					Cexception EXCobject;
					EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
					throw(EXCobject);
				}
				else
				{
					char* sBuffer1 = _strdup(sToken1);

					if (FILfind_occurence(sBuffer1, '+') == true || FILfind_occurence(sBuffer1, ',') == true)
					{
						Cexception EXCobject;
						EXCobject.EXCset_value(FILexceptionValueOfTheMatrixIsNotLogical);
						delete[] sBuffer1;
						throw(EXCobject);
					}
					else if (FILisItANumber(sBuffer1) == true) //just to check if we have a number
					{
						pFILmatrixContent[iFILnbOfColumns*iLoopFILnbOfRows] = atof(sBuffer1); //to convert the char* into a double value
						delete[] sBuffer1;
					}
					else
					{
						Cexception EXCobject;
						EXCobject.EXCset_value(FILexceptionValueOfTheMatrixIsNotLogical);
						delete[] sBuffer1;
						throw(EXCobject);
					}

				}

				for (int iLoopFILnbOfColumns = 1; iLoopFILnbOfColumns < iFILnbOfColumns; iLoopFILnbOfColumns++)
				{
					sToken2 = strtok_s(nullptr, sSeparator, &sNext_token1);
					if (sToken2 == nullptr)
					{
						Cexception EXCobject;
						EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
						throw(EXCobject);
					}

					char* sBuffer2 = _strdup(sToken2);

					if (FILfind_occurence(sBuffer2, '+') == true || FILfind_occurence(sBuffer2, ',') == true) //because +3.0 is considered has a number, and I'm not sure that +3 is working for a double number...
					{
						Cexception EXCobject;
						EXCobject.EXCset_value(FILexceptionValueOfTheMatrixIsNotLogical);
						delete[] sBuffer2;
						throw(EXCobject);
					}
					else if (FILisItANumber(sBuffer2) == true) //just to check if we have a number
					{
						pFILmatrixContent[iLoopFILnbOfColumns + iFILnbOfColumns * iLoopFILnbOfRows] = atof(sBuffer2); //to convert the char* into a double value
						delete[] sBuffer2;
					}
					else
					{
						Cexception EXCobject;
						EXCobject.EXCset_value(FILexceptionValueOfTheMatrixIsNotLogical);
						delete[] sBuffer2;
						throw(EXCobject);
					}
				}

				sToken2 = strtok_s(nullptr, sSeparator, &sNext_token1); //IF AFTER THE FULL READING OF THE COLUMN, ONE ELEMENT IS MISSING...
				if (sToken2 != nullptr)
				{
					Cexception EXCobject;
					EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
					throw(EXCobject);
				}
			}
		}

		bool bTagIsFound = false;
		time(&tStart); //restart the time
		double tTimeTaken = 0;
		while (tTimeTaken > 0.0016) //If after 0.0016s, we still can't read anything, we consider that the file is simply empty and we don't read it.
		{
			time(&tEnd);
			tTimeTaken = double(tEnd - tStart) / double(CLOCKS_PER_SEC);

			file.getline(sBuffer0, 200); //We read the last line of the file

			char sSeparator[] = " ";
			char* sToken1 = nullptr;
			char* sToken2 = nullptr;
			char* sNext_token1 = nullptr;

			sToken1 = strtok_s(sBuffer0, sSeparator, &sNext_token1);

			if (sToken1 != nullptr && strcmp(sToken1, "]") == 0)
			{
				bTagIsFound = true; //We found the char ']'
			}

			else if (strcmp(sBuffer0, "") != 0) //else if the line is composed of something (which is not a space)
			{
				Cexception EXCobject;
				EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
				throw(EXCobject);
			}

		}
		if (tTimeTaken > 0.0016 && bTagIsFound == false) //If after 0.0016s, if t he tag ] is not read, we consider that the file is simply empty and we don't read it.
		{
			Cexception EXCobject;
			EXCobject.EXCset_value(FILexceptionStructureOfTheFileIncorrect);
			throw(EXCobject);
		}

	}
	file.close(); //we close the file

	return true;
}
