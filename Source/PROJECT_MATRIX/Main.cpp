/**
 * @authors Jeremy SEUBE and Killian SINGLA
 * @date 2021
 * @copyright GNU Public Licence
 */


//TOOL FOR CHECKING MEMORY LEAKS
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifdef _DEBUG
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif

#include"Cexception.h"
#include"Cmatrix.h"
#include"Cfile_matrix.h"
#include<iostream>
#include<fstream>
using namespace std;

int main(int argc, char* argv[])
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); //checking for memory leaks
	cout << "--------------------------------------- BEGINNING OF THE V1 PROGRAM ---------------------------------------\n\n\n" << endl;

	if (argc > 1) 
	{
		Cmatrix<double> matrix;

		Cfile_matrix* pFILarrayOfAllFiles = new Cfile_matrix[argc - 1];
		for (int iLoopElementsInArgv = 0; iLoopElementsInArgv < argc - 1; iLoopElementsInArgv++)
		{
			pFILarrayOfAllFiles[iLoopElementsInArgv].FILset_file_name(argv[iLoopElementsInArgv + 1]); //iLoopElementsInArgv+1 because argv[0] = name of the .exe
		}

		cout << " ------------------------------ PARSING OF YOUR DIFFERENT FILES ------------------------------\n" << endl;
		int iCountFilesThatAreOK = argc-1;
		int iIndexOfFilesThatAreOK = 0;
		int* pArrayIndexInTheTabFilesOK = new int[argc - 1]; //this array reprents the index of the pFILarrayOfAllFiles that represent files with a correct reading and FILparsing
		for (int iLoopElements = 0; iLoopElements < argc - 1; iLoopElements++)
		{
			try
			{
				pFILarrayOfAllFiles[iLoopElements].FILparsing();
				pArrayIndexInTheTabFilesOK[iIndexOfFilesThatAreOK] = iLoopElements;
				cout << "\n FILE MATRIX " << pFILarrayOfAllFiles[iLoopElements].FILget_file_name() << " : Reading and parsing okay\n" << endl;
				iIndexOfFilesThatAreOK++;
			}
			catch(Cexception EXCobject)
			{
				cout << "\n FILE MATRIX " << pFILarrayOfAllFiles[iLoopElements].FILget_file_name() << " : FAILURE\n" << endl;
				iCountFilesThatAreOK--;
				cout << "	\nParsing of your file => the exception number " << EXCobject.EXCget_value() << " has been thrown : \n" << endl;
				switch (EXCobject.EXCget_value())
				{
					case 0:
						cout << "		Your file is not readable. Check your filename, verify if the file is in the same directory as the .exe, be careful when you specifying arguments on the command prompt.\n" << endl;
						break;
					case 1:
						cout << "		Your file is incorrect according to the structure imposed. \n		Maybe you try tu put too much spaces between lines and the parser has decided to stop reading after " << endl;
						cout << "		a certain time or maybe you misspelled something.\n\n		The structure is in French, and must be as following :\n\n		TypeMatrice=<type_base_C>\n		NBLignes=<Nombre_de_lignes_de_la_matrice>\n		NBColonnes=<Nombre_de_colonnes_de_la_matrice>\n" << endl;
						cout <<"		Matrice=[\n		<Ligne_1:autant d elements que de colonnes>\n		...\n		<Ligne_n:autant d elements que de colonnes>\n		]\n" << endl;
						break;
					case 2:
						cout << "		You tried to specify a negative number/a decimal number (with , or .) of columns/rows.\n		We talk about Matrix here, we need an integer value for the number of rows and columns (such as 1 or 2 for example).\n" << endl;
						break;
					case 3:
						cout << "		Your values aren't logical with what a Matrix must be : instead of , use . (2,0 => 2.0), instead of +3 just write 3.\n";
						break;

					default:
						cout << "		Exception argument not defined " << endl;
				}
			}
		}

		Cmatrix<double>* pMATarrayOfAllMatricesGeneratedByFiles = new Cmatrix<double>[iCountFilesThatAreOK]; //There's countFileOk Files that are okay, so we reserved countFileOk matrixes in tabMatrix
		int* pArrayIndexInTheTabMatricesOK = new int[iCountFilesThatAreOK]; //this array represents the index of the tabMatrix that represent Cmatrix that are well created with the file

		cout << "\n------------------------------ GENERATION OF THE MATRICES WITH YOUR FILES ------------------------------\n" << endl;

		int iCountMatricesThatAreOK = iCountFilesThatAreOK; //By default we'll have as much files that are OK as Matrix
		int iIndexOfMatricesThatAreOK = 0;
		for (int iLoopElements = 0; iLoopElements < iCountFilesThatAreOK; iLoopElements++)
		{
			try
			{
				Cmatrix<double> newMatrix(pFILarrayOfAllFiles[pArrayIndexInTheTabFilesOK[iLoopElements]]);
				pArrayIndexInTheTabMatricesOK[iIndexOfMatricesThatAreOK] = iLoopElements;
				pMATarrayOfAllMatricesGeneratedByFiles[iLoopElements] = newMatrix;
				cout << "\n" << pFILarrayOfAllFiles[pArrayIndexInTheTabFilesOK[iLoopElements]].FILget_file_name() << pMATarrayOfAllMatricesGeneratedByFiles[iLoopElements];
				iIndexOfMatricesThatAreOK++;
			}
			catch (Cexception EXCobject)
			{
				iCountMatricesThatAreOK--;
				cout << "	\nCreation of Cmatrix with your files : the exception number " << EXCobject.EXCget_value() << " has been thrown : " << endl;

				switch (EXCobject.EXCget_value())
				{
					case 2:
						cout << "		Your numbers of rows or columns (or both) are not compatible. Maybe you tried to put a negative value." << endl;

					case 3:
						cout << "		This constructor is only made to create Cmatrix<double>. Change the type and check your values !\n" << endl;
						break;

					default:
						cout << "		Exception argument not defined.\n" << endl;
				}
			}
		}

		if (iCountMatricesThatAreOK > 0)
		{
			cout << "\n------------------------------ CALCULATIONS WITH YOUR MATRICES ------------------------------" << endl;

			cout << "\n\nPlease enter a c constant value : ";
			long double c;
			cin >> c;

			for (int iLoopMatrices = 0; iLoopMatrices < iCountMatricesThatAreOK; iLoopMatrices++)
			{
				try // Matrix[iLoopMatrices]*c
				{
					Cmatrix<double> MATtemporaryMatrixMULTIPLICATION = pMATarrayOfAllMatricesGeneratedByFiles[pArrayIndexInTheTabMatricesOK[iLoopMatrices]]*c;
					cout << pMATarrayOfAllMatricesGeneratedByFiles[pArrayIndexInTheTabMatricesOK[iLoopMatrices]] << "	*"<< c << " = " << MATtemporaryMatrixMULTIPLICATION << endl;
				}
				catch (Cexception EXCobject)
				{
					cout << "	\nAll of your Cmatrix *c => the exception number " << EXCobject.EXCget_value() << " has been thrown " << endl;
					switch (EXCobject.EXCget_value())
					{
						default:
							cout << "		Exception argument not defined.\n" << endl;
					}
				}

				
			}

			for (int iLoopMatrices = 0; iLoopMatrices < iCountMatricesThatAreOK; iLoopMatrices++)
			{
				try // Matrix[iLoopMatrices]/c
				{
					Cmatrix<double> MATtemporaryMatrixDIVISION = pMATarrayOfAllMatricesGeneratedByFiles[pArrayIndexInTheTabMatricesOK[iLoopMatrices]] / c;
					cout << pMATarrayOfAllMatricesGeneratedByFiles[pArrayIndexInTheTabMatricesOK[iLoopMatrices]] << "	/ " << c << " = " << MATtemporaryMatrixDIVISION << endl;
				}
				catch (Cexception EXCobject)
				{
					cout << pMATarrayOfAllMatricesGeneratedByFiles[pArrayIndexInTheTabMatricesOK[iLoopMatrices]] << "	/ " << c << " => ";
					cout << "the exception number " << EXCobject.EXCget_value() << " has been thrown \n" << endl;
					switch (EXCobject.EXCget_value())
					{
						case 6:
							cout << "		Your calculation is impossible to make. It's possible when you tried to divide a Cmatrix by 0 for example.\n";
							break;

						default:
							cout << "		Exception argument not defined.\n" << endl;
					}
				}

			}

			Cmatrix<double> MATtemporaryMatrixSUM = pMATarrayOfAllMatricesGeneratedByFiles[pArrayIndexInTheTabMatricesOK[0]];
			try //Sum
			{
				for (int iLoopMatrices = 1; iLoopMatrices < iCountMatricesThatAreOK; iLoopMatrices++)
				{
					MATtemporaryMatrixSUM = MATtemporaryMatrixSUM + pMATarrayOfAllMatricesGeneratedByFiles[pArrayIndexInTheTabMatricesOK[iLoopMatrices]];
				}
				cout << " \nSum of all of your Cmatrix : " << MATtemporaryMatrixSUM << endl;
			}
			catch (Cexception EXCobject)
			{
				cout << "	\nSum of all of your Cmatrix => the exception number " << EXCobject.EXCget_value() << " has been thrown " << endl;

				switch (EXCobject.EXCget_value())
				{
					case 1:
						cout << "		You tried to make one Cmatrix + or - with another Cmatrix who has different dimensions.\n ";
						break;

					default:
						cout << "		Exception argument not defined.\n" << endl;
				}

			}

			Cmatrix<double> MATtemporaryMatrixALTERNATESUM = pMATarrayOfAllMatricesGeneratedByFiles[pArrayIndexInTheTabMatricesOK[0]];
			try //Alternate Sum
			{
				for (int iLoopMatrices = 1; iLoopMatrices < iCountMatricesThatAreOK; iLoopMatrices++)
				{
					if (iLoopMatrices % 2 == 0)
					{
						MATtemporaryMatrixALTERNATESUM = MATtemporaryMatrixALTERNATESUM + pMATarrayOfAllMatricesGeneratedByFiles[pArrayIndexInTheTabMatricesOK[iLoopMatrices]];
					}
					else
					{
						MATtemporaryMatrixALTERNATESUM = MATtemporaryMatrixALTERNATESUM - pMATarrayOfAllMatricesGeneratedByFiles[pArrayIndexInTheTabMatricesOK[iLoopMatrices]];
					}
				}
				cout << " Alternate sum (M1-M2+M3-M4...) of all of your Cmatrix : " << MATtemporaryMatrixALTERNATESUM << endl;
			}
			catch (Cexception EXCobject)
			{
				cout << "	\nAlternate sum of all of your Cmatrix => the exception number " << EXCobject.EXCget_value() << " has been thrown " << endl;

				switch (EXCobject.EXCget_value())
				{
					case 1:
						cout << "		You tried to make one Cmatrix + or - with another Cmatrix who has different dimensions.\n ";
						break;

					default:
						cout << "		Exception argument not defined.\n" << endl;
				}
			}

			Cmatrix<double> MATtemporaryMatrixPRODUCT = pMATarrayOfAllMatricesGeneratedByFiles[pArrayIndexInTheTabMatricesOK[0]];
			try //Product
			{
				for (int iLoopMatrices = 1; iLoopMatrices < iCountMatricesThatAreOK; iLoopMatrices++)
				{
					MATtemporaryMatrixPRODUCT = MATtemporaryMatrixPRODUCT*pMATarrayOfAllMatricesGeneratedByFiles[pArrayIndexInTheTabMatricesOK[iLoopMatrices]];
				}
				cout << " Product of all of your Cmatrix : " << MATtemporaryMatrixPRODUCT;
			}
			catch (Cexception EXCobject)
			{
				cout << "	\nProduct of all of your Cmatrix => the exception number " << EXCobject.EXCget_value() << " has been thrown " << endl;
				switch (EXCobject.EXCget_value())
				{
					case 5:
						cout << "		You tried to make one Cmatrix 1 * Cmatrix 2 and the number of columns of Cmatrix 1 is not equal to the number of rows of Cmatrix 2.\n";
						break;

					default:
						cout << "		Exception argument not defined.\n" << endl;
				}
			}

			cout << "\n\n------------------------------------------ END OF THE V1 PROGRAM ------------------------------------------\n\n" << endl;
		}
		else
		{
			cout << " Then I can't do nothing (V1 PROGRAM)... " << endl;
			cout << "\n\n------------------------------------------ END OF THE V1 PROGRAM ------------------------------------------\n\n" << endl;

		}

		//we don't want memory leaks !!!!!!
		delete[] pFILarrayOfAllFiles;
		delete[] pMATarrayOfAllMatricesGeneratedByFiles;
		delete[] pArrayIndexInTheTabFilesOK;
		delete[] pArrayIndexInTheTabMatricesOK;

		cout << "\n\n--------------------------------------- BEGINNING OF THE V2 PROGRAM ---------------------------------------" << endl;
		cout << "\n-------------------- CHOLESKY'S FACTORIZATION OF AN SYMETRICAL POSITIVE DEFINITVE MATRIX -------------------\n" << endl;

		cout << "	FIRST CASE : the matrix is not symmetrical (and then the factorization will not work) " << endl;
		cout << "		\n\nCreation of an matrix which is not symmetrical... " << endl;
		Cmatrix<double> MATnotsym(2, 2, nullptr);
		MATnotsym.MATget_array2D()[0][0] = 5;
		MATnotsym.MATget_array2D()[0][1] = 1;
		MATnotsym.MATget_array2D()[1][0] = 4;
		MATnotsym.MATget_array2D()[1][1] = 3;

		cout << "			" << MATnotsym;

		cout << "		Let's try the Factorization... " << endl;

		try
		{
			MATnotsym.MATfactorization_Cholesky();
		}
		catch (Cexception EXCobject)
		{
			cout << "\n\n			Factorization (Cholesky's method) of your Cmatrix => the exception number " << EXCobject.EXCget_value() << " has been thrown " << endl;
			switch (EXCobject.EXCget_value())
			{
				case 6:
					cout << "			You tried to use the factorization of Choleskey with a matrix which is not symmetrical.\n\n\n" << endl;
					break;

				case 7:
					cout << "			You tried to use the factorization of Choleskey with a matrix which is not positive definite.\n\n\n" << endl;
					break;

				default:
					cout << "		Exception argument not defined.\n" << endl;
			}
		}


		cout << "	SECOND CASE : the matrix is symmetrical and positive definite (and then the factorization will work) " << endl;
		cout << "		\n\nCreation of an symmetrical positive definite matrix... " << endl;

		Cmatrix<double> MATsym(4,4, nullptr);

		MATsym.MATget_array2D()[0][0] = 1;
		MATsym.MATget_array2D()[0][1] = 1;
		MATsym.MATget_array2D()[0][2] = 1;
		MATsym.MATget_array2D()[0][3] = 1;
		MATsym.MATget_array2D()[1][0] = 1;
		MATsym.MATget_array2D()[1][1] = 5;
		MATsym.MATget_array2D()[1][2] = 5;
		MATsym.MATget_array2D()[1][3] = 5;
		MATsym.MATget_array2D()[2][0] = 1;
		MATsym.MATget_array2D()[2][1] = 5;
		MATsym.MATget_array2D()[2][2] = 14;
		MATsym.MATget_array2D()[2][3] = 14;
		MATsym.MATget_array2D()[3][0] = 1;
		MATsym.MATget_array2D()[3][1] = 5;
		MATsym.MATget_array2D()[3][2] = 14;
		MATsym.MATget_array2D()[3][3] = 15;

		cout << "			" << MATsym;

		cout << "		Let's try the Factorization... " << endl;

		try
		{
			Cmatrix<double> MATtriangular;
			Cmatrix<double> MATtriangularTransposed;
			Cmatrix<double> MATverification;
			cout << "\n			>Generation of the triangular matrix associated..." << endl;
			MATtriangular = MATsym.MATfactorization_Cholesky();
			MATtriangularTransposed = MATtriangular.MATtranspose_matrix();
			MATverification = MATtriangular * MATtriangularTransposed;
			cout << "\n\nTHE MATRIX SYMMETRICAL AND POSITIVE DEFINITE \n" << MATverification << "\n=  THE TRIANGULAR MATRIX\n" << MATtriangular << "\n * " << " THE TRANSPOSED VERSION OF THE TRIANGULAR MATRIX\n" << MATtriangularTransposed << endl;
		}
		catch (Cexception EXCobject)
		{
			cout << "\n\n			Factorization (Cholesky's method) of your Cmatrix => the exception number " << EXCobject.EXCget_value() << " has been thrown " << endl;
			switch (EXCobject.EXCget_value())
			{
				case 6:
					cout << "			You tried to use the factorization of Choleskey with a matrix which is not symmetrical.\n\n\n" << endl;
					break;

				case 7:
					cout << "			You tried to use the factorization of Choleskey with a matrix which is not positive definite.\n\n\n" << endl;
					break;

				default:
					cout << "		Exception argument not defined.\n" << endl;
			}
		}

		return 0;
	}
	else
	{
		cout << " I can't do nothing if you give me nothing to do...\n Relaunch the executable and specify as parameters the names of the matrix txt files (they must be in the same directory as the .exe).\n Keep in mind that these have an imposed structure to respect. " << endl;
		cout << "\n\n------------------------------------------ END OF THE V1 PROGRAM ------------------------------------------\n\n" << endl;
		cout << "\n\n--------------------------------------- BEGINNING OF THE V2 PROGRAM ---------------------------------------" << endl;
		cout << "\n-------------------- CHOLESKY'S FACTORIZATION OF AN SYMETRICAL POSITIVE DEFINITVE MATRIX -------------------\n" << endl;

		cout << "	FIRST CASE : the matrix is not symmetrical (and then the factorization will not work) " << endl;
		cout << "		\n\nCreation of an matrix which is not symmetrical... " << endl;
		Cmatrix<double> MATnotsym(2, 2, nullptr);
		MATnotsym.MATget_array2D()[0][0] = 5;
		MATnotsym.MATget_array2D()[0][1] = 1;
		MATnotsym.MATget_array2D()[1][0] = 4;
		MATnotsym.MATget_array2D()[1][1] = 3;

		cout << "			" << MATnotsym;

		cout << "		Let's try the Factorization... " << endl;

		try
		{
			MATnotsym.MATfactorization_Cholesky();
		}
		catch (Cexception EXCobject)
		{
			cout << "\n\n			Factorization (Cholesky's method) of your Cmatrix => the exception number " << EXCobject.EXCget_value() << " has been thrown " << endl;
			switch (EXCobject.EXCget_value())
			{
				case 6:
					cout << "			You tried to use the factorization of Choleskey with a matrix which is not symmetrical.\n\n\n" << endl;
					break;

				case 7:
					cout << "			You tried to use the factorization of Choleskey with a matrix which is not positive definite.\n\n\n" << endl;
					break;

				default:
					cout << "		Exception argument not defined.\n" << endl;
			}
		}


		cout << "	SECOND CASE : the matrix is symmetrical and positive definite (and then the factorization will work) " << endl;
		cout << "		\n\nCreation of an symmetrical positive definite matrix... " << endl;

		Cmatrix<double> MATsym(4, 4, nullptr);

		MATsym.MATget_array2D()[0][0] = 1;
		MATsym.MATget_array2D()[0][1] = 1;
		MATsym.MATget_array2D()[0][2] = 1;
		MATsym.MATget_array2D()[0][3] = 1;
		MATsym.MATget_array2D()[1][0] = 1;
		MATsym.MATget_array2D()[1][1] = 5;
		MATsym.MATget_array2D()[1][2] = 5;
		MATsym.MATget_array2D()[1][3] = 5;
		MATsym.MATget_array2D()[2][0] = 1;
		MATsym.MATget_array2D()[2][1] = 5;
		MATsym.MATget_array2D()[2][2] = 14;
		MATsym.MATget_array2D()[2][3] = 14;
		MATsym.MATget_array2D()[3][0] = 1;
		MATsym.MATget_array2D()[3][1] = 5;
		MATsym.MATget_array2D()[3][2] = 14;
		MATsym.MATget_array2D()[3][3] = 15;

		cout << "			" << MATsym;

		cout << "		Let's try the Factorization... " << endl;

		try
		{
			Cmatrix<double> MATtriangular;
			Cmatrix<double> MATtriangularTransposed;
			Cmatrix<double> MATverification;
			cout << "\n			>Generation of the triangular matrix associated..." << endl;
			MATtriangular = MATsym.MATfactorization_Cholesky();
			MATtriangularTransposed = MATtriangular.MATtranspose_matrix();
			MATverification = MATtriangular * MATtriangularTransposed;
			cout << "\n\nTHE MATRIX SYMMETRICAL AND POSITIVE DEFINITE \n" << MATverification << "\n=  THE TRIANGULAR MATRIX\n" << MATtriangular << "\n * " << " THE TRANSPOSED VERSION OF THE TRIANGULAR MATRIX\n" << MATtriangularTransposed << endl;
		}
		catch (Cexception EXCobject)
		{
			cout << "\n\n			Factorization (Cholesky's method) of your Cmatrix => the exception number " << EXCobject.EXCget_value() << " has been thrown " << endl;
			switch (EXCobject.EXCget_value())
			{
				case 6:
					cout << "			You tried to use the factorization of Choleskey with a matrix which is not symmetrical.\n\n\n" << endl;
					break;

				case 7:
					cout << "			You tried to use the factorization of Choleskey with a matrix which is not positive definite.\n\n\n" << endl;
					break;

				default:
					cout << "		Exception argument not defined.\n" << endl;
			}
		}

		return 0;
	}
}
