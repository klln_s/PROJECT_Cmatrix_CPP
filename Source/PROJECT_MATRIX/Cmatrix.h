#pragma once

//TOOL FOR CHECKING MEMORY LEAKS
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifdef _DEBUG
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif

#include<iostream>
#include"Cfile_matrix.h"
using namespace std;

#define MATexceptionDifferentDimensions 1
#define MATexceptionSpecifiedNumbersOfColumnsOrRowsAreNotCompatible 2
#define MATexceptionTypeChosenAreNotCompatibleWithThisConstructor 3
#define MATexceptionNumberOfColumnsOfMatrix1IsNotCompatibleWithNumberOfRowsOfMatrix2 4
#define MATexceptionCalculationIsImpossible 5

#define MATexceptionMatrixNotSymmetrical 6
#define MATexceptionFactorizationCholeskyMatrixNotPositiveDefinite 7

template <class type> 
class Cmatrix
{

	protected:

		unsigned int iNbColumns;
		unsigned int iNbRows;
		type** pTYParray2D;

	public:

		/**
		 * @param[in] nothing
		 * @pre nothing
		 * @return nothing
		 * @brief The current object is initialized with the value by default parameters (nb colums = 0, nb rows = 0, pTYParray2d = nullptr).
		 */
		Cmatrix<type>();


		/**
		 * @param[in]  one unsigned integer value corresponding to the numbers of rows, one unsigned integer value corresponding to the numbers of columns, one pointer of an 2D array of any type
		 * @pre The number of rows and columns must corresponding to the actual dimensions of the 2D array if the pointer of the 2D array isn't nullptr
		 * @return nothing
		 * @brief The current object is initialized with the content of the input Cmatrix of any type
		 */
		Cmatrix<type>(unsigned int iNbRowsPARAM, unsigned int iNbColumnsPARAM, type** pTYParray2DPARAM);
		/**
		 * @param[in] one const reference of a Cmatrix of any type
		 * @pre nothing
		 * @return nothing
		 * @brief The current object is initialized with the content of the input Cmatrix of any type 
		 */
		Cmatrix<type>(const Cmatrix<type>& MATparam);


		/**
		 * @param[in] A reference to file_matrix
		 * @pre nothing
		 * @return nothing
		 * @brief The current object is initialized with the content of the file_matrix
		 */
		Cmatrix<type>(Cfile_matrix& FIMparam);


		/**
		 * @param[in] nothing
		 * @pre nothing
		 * @return nothing
		 * @brief The current object is deallocated
		 */
		~Cmatrix<type>();


		/**
		 * @param[in] nothing
		 * @pre nothing
		 * @return A Cmatrix of any type
		 * @brief A new Cmatrix of any type is created and is the transposed matrix of the calling matrix
		 */
		Cmatrix<type> MATtranspose_matrix();

		/**
		 * @param[in] nothing
		 * @pre nothing
		 * @return A triangular matrix of any type
		 * @brief A new Cmatrix of any type is created and is the triangular, factorized (with the Cholesky's method) of the calling matrix
		 */
		Cmatrix<type> MATfactorization_Cholesky();

		/**
		 * @param[in]  one const reference of a Cmatrix of any type
		 * @pre nothing
		 * @return the reference of the current Cmatrix
		 * @brief The current object is modified : the number of rows and columns, and the values in the array are replaced by the number of rows and columns, and the values in the array of the parameter, which is a const Cmatrix of any type.
		 */
		Cmatrix<type>& operator=(const Cmatrix<type>& MATparam);


		/**
		 * @param[in] one const reference of a Cmatrix of any type
		 * @pre nothing
		 * @return the Cmatrix of the result of the addition
		 * @brief Add the values of the current Cmatrix with the values of the Cmatrix in parameter.
		 */
		Cmatrix<type> operator+(const Cmatrix<type>& MATparam);


		/**
		 * @param[in] one const reference of a Cmatrix of any type
		 * @pre nothing
		 * @return the Cmatrix of the result of the substraction
		 * @brief Substract the values of the current Cmatrix by the value of the Cmatrix in parameter.
		 */
		Cmatrix<type> operator-(const Cmatrix<type>& MATparam);

		/**
		 * @param[in] one const reference of a Cmatrix of any type
		 * @pre nothing
		 * @return the Cmatrix of the result of the multiplication
		 * @brief Multiplies the values of the current Cmatrix by the value of the Cmatrix in parameter.
		 */
		Cmatrix<type> operator*(const Cmatrix<type>& MATparam);

		/**
		 * @param[in] nothing
		 * @pre nothing
		 * @return The assigned value of the data mumber pTYParray2D of the current object
		 * @brief nothing
		 */
		type** MATget_array2D();

		/**
		 * @param[in] nothing
		 * @pre nothing
		 * @return The assigned value of the data mumber iNbColumns of the current object
		 * @brief nothing
		 */
		unsigned int MATget_nb_columns();


		/**
		 * @param[in] nothing
		 * @pre nothing
		 * @return The assigned value of the data mumber iNbRows of the current object
		 * @brief nothing
		 */
		unsigned int MATget_nb_rows();

		/**
		 * @param[in] one unsigned integer value corresponding to the numbers of rows, one unsigned integer value corresponding to the numbers of columns
		 * @pre nothing
		 * @return nothing
		 * @brief We assigned a value to data mumber iEXCvalue of the current object
		 */
		void MATset_array2D(unsigned int iNbColumnsPARAM, unsigned int iNbRowsPARAM);


		/**
		 * @param[in] the number of columns
		 * @pre nothing
		 * @return nothing
		 * @brief sets the number of columns of the array of the Cmatrix by the value in parameter.
		 */
		void MATset_nb_columns(unsigned int iNbColumnsPARAM);


		/**
		 * @param[in] the number of rows
		 * @pre nothing
		 * @return nothing
		 * @brief sets the number of rows of the array of the Cmatrix by the value in parameter.
		 */
		void MATset_nb_rows(unsigned int iNbRowsPARAM);
};


/**
 * @param[in] one reference of an ostream operator, one reference of a Cmatrix of any type
 * @pre nothing
 * @return A reference of an ostream operator
 * @brief The Cmatrix of any type is printed on the console
 */
template<class type>
ostream& operator<<(ostream& stream, Cmatrix<type>& MATparam)
{
	cout << endl;

	for (unsigned int iLoopRows = 0; iLoopRows < MATparam.MATget_nb_rows(); iLoopRows++)
	{
		stream << "[ ";
		for (unsigned int iLoopColumns = 0; iLoopColumns < MATparam.MATget_nb_columns(); iLoopColumns++)
		{
			stream << MATparam.MATget_array2D()[iLoopRows][iLoopColumns] << " ";
		}
		stream << "]" << endl;
	}

	return stream;
}


/**
 * @param[in] one reference of an istream operator, one reference of a Cmatrix of any type
 * @pre nothing
 * @return A reference of an istream operator
 * @brief The Cmatrix of any type is entered with the console
 */
template<class type>
istream& operator>>(istream& stream, Cmatrix<type>& MATparam)
{
	int iValueNbRows;
	int iValueNbColumns;
	cout << "\n Number of rows wanted : ";
	stream >> iValueNbRows;
	cout << " Number of columns wanted : ";
	stream >> iValueNbColumns;
	cout << endl;

	if (iValueNbColumns < 0 || iValueNbRows < 0)
	{
		Cexception EXCobject;
		EXCobject.EXCset_value(MATexceptionSpecifiedNumbersOfColumnsOrRowsAreNotCompatible);
		throw(EXCobject);
	}
	else
	{
		MATparam.MATset_nb_rows(iValueNbRows);
		MATparam.MATset_nb_columns(iValueNbColumns);
	}
	

	if (MATparam.MATget_array2D() != nullptr)
	{
		for (unsigned int iLoopRows = 0; iLoopRows < MATparam.MATget_nb_rows(); iLoopRows++)
		{
			delete[]  MATparam.MATget_array2D()[iLoopRows];
		}
		delete[] MATparam.MATget_array2D();
	}
	
	MATparam.MATset_array2D(MATparam.MATget_nb_columns(), MATparam.MATget_nb_rows());

	for (unsigned int iLoopRows = 0; iLoopRows < MATparam.MATget_nb_rows(); iLoopRows++)
	{
		for (unsigned int iLoopColumns = 0; iLoopColumns < MATparam.MATget_nb_columns(); iLoopColumns++)
		{
			cout << "	Element R" << iLoopRows + 1 << "C" << iLoopColumns + 1 << " : ";
			stream >> MATparam.MATget_array2D()[iLoopRows][iLoopColumns];
		}
	}

	return stream;
}


/**
 * @param[in] one number of any type (type1) and one Cmatrix of any type (type2)
 * @pre nothing
 * @return the Cmatrix of the result of the multiplication
 * @brief multiplies the values in the array of the Cmatrix of any type (type 2) by the value of any type (type1) in parameter.
 */
template <class type1, class type2>
Cmatrix<type2> operator*(type1 TYPparam, Cmatrix<type2> MATparam) //C*Matrix
{
	Cmatrix<type2> MATres(MATparam);
	for (unsigned int iLoopRows = 0; iLoopRows < MATres.MATget_nb_rows(); iLoopRows++)
	{
		for (unsigned int iLoopColumns = 0; iLoopColumns < MATres.MATget_nb_columns(); iLoopColumns++)
		{
			MATres.MATget_array2D()[iLoopRows][iLoopColumns] = MATparam.MATget_array2D()[iLoopRows][iLoopColumns] * TYPparam;
		}
	}
	return MATres;
}


/**
 * @param[in] one Cmatrix of any type (type 1) and one number of any type (type 2)
 * @pre nothing
 * @return the Cmatrix of the result of the multiplication
 * @brief multiplies the values in the array of the Cmatrix of any type (type1) by the value of any type (type2) in parameter.
 */
template <class type1, class type2>
Cmatrix<type1> operator*(Cmatrix<type1> MATparam, type2 TYPparam) //Matrix * C
{
	Cmatrix<type1> MATres(MATparam);
	for (unsigned int iLoopRows = 0; iLoopRows < MATres.MATget_nb_rows(); iLoopRows++)
	{
		for (unsigned int iLoopColumns = 0; iLoopColumns < MATres.MATget_nb_columns(); iLoopColumns++)
		{
			MATres.MATget_array2D()[iLoopRows][iLoopColumns] = MATparam.MATget_array2D()[iLoopRows][iLoopColumns] * TYPparam;
		}
	}
	return MATres;
}


/**
 * @param[in] one Cmatrix of any type (type 1) and one number of any type (type 2)
 * @pre nothing
 * @return the Cmatrix of the result of the division
 * @brief divides the values in the array of the Cmatrix of any type (type 1) by the value in parameter of any type (type 2).
 */
template<class type1, class type2>
Cmatrix<type1> operator/(Cmatrix<type1> MATparam, type2 TYPparam)
{
	if (TYPparam != 0)
	{
		Cmatrix<type1> MATres(MATparam);
		for (unsigned int iLoopRows = 0; iLoopRows < MATres.MATget_nb_rows(); iLoopRows++)
		{
			for (unsigned int iLoopColumns = 0; iLoopColumns < MATres.MATget_nb_columns(); iLoopColumns++)
			{
				MATres.MATget_array2D()[iLoopRows][iLoopColumns] = MATparam.MATget_array2D()[iLoopRows][iLoopColumns] / TYPparam;
			}
		}
			return MATres;
	}
	else //division by 0 is impossible
	{
		Cexception EXCobject;
		EXCobject.EXCset_value(MATexceptionCalculationIsImpossible);
		throw(EXCobject);
	}
	
}

#include"Cmatrix.cpp" //we need to include the file here because it musn't be compiled before the launching of the program