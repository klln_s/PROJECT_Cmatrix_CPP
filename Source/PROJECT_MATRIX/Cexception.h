#pragma once

class Cexception
{

	private:

		int iEXCvalue;

	public:

		/**
		 * @param[in] nothing
		 * @pre nothing
		 * @return nothing
		 * @brief The current object is initialized with the value by default -1.
		 */
		Cexception();


		/**
		 * @param[in] integer value
		 * @pre nothing
		 * @return nothing
		 * @brief The current object is initialiazed with the value precised in parameter
		 */
		Cexception(int iEXCarg);


		/**
		 * @param[in] nothing
		 * @pre nothing
		 * @return nothing
		 * @brief The current object is deallocated
		 */
		~Cexception();


		/**
		 * @param[in] an integer value
		 * @pre The value in parameter must be > 0 and be an integer value
		 * @return nothing
		 * @brief We assigned a value to data mumber iEXCvalue of the current object
		 */
		void EXCset_value(int EXCvalue);


		/**
		 * @param[in] nothing
		 * @pre nothing
		 * @return The assigned value of the data mumber iEXCvalue of the current object
		 * @brief We get the value to date mumber iEXCvalue of the current object
		 */
		int EXCget_value();

};

