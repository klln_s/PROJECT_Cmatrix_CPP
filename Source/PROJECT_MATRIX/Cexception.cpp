#include"Cexception.h"

/**
 * @param[in] nothing
 * @pre nothing
 * @return nothing
 * @brief The current object is initialized with the value by default -1.
*/
Cexception::Cexception()
{
	iEXCvalue = -1;
}


/**
 * @param[in] integer value
 * @pre nothing
 * @return nothing
 * @brief The current object is initialiazed with the value precised in parameter
*/
Cexception::Cexception(int iEXCarg)
{
	iEXCvalue = iEXCarg;
}


/**
 * @param[in] nothing
 * @pre nothing
 * @return nothing
 * @brief The current object is deallocated
*/
Cexception::~Cexception()
{ }


/**
 * @param[in] an integer value
 * @pre The value in parameter must be > 0 and be an integer value
 * @return nothing
 * @brief We assigned a value to data mumber iEXCvalue of the current object
*/
void Cexception::EXCset_value(int EXCvalue)
{
	iEXCvalue = EXCvalue;
}


/**
 * @param[in] nothing
 * @pre nothing
 * @return The assigned value of the data mumber iEXCvalue of the current object
 * @brief
*/
int Cexception::EXCget_value()
{
	return iEXCvalue;
}
