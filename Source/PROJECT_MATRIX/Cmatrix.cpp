#include "Cmatrix.h"

/**
 * @param[in] nothing
 * @pre nothing
 * @return nothing
 * @brief The current object is initialized with the value by default parameters (nb colums = 0, nb rows = 0, pTYParray2d = nullptr).
 */
template<class type>
Cmatrix<type>::Cmatrix()
{
	iNbColumns = 0;
	iNbRows = 0;
	pTYParray2D = nullptr;
}


/**
 * @param[in]  one unsigned integer value corresponding to the numbers of rows, one unsigned integer value corresponding to the numbers of columns, one pointer of an 2D array of any type
 * @pre The number of rows and columns must corresponding to the actual dimensions of the 2D array if the pointer of the 2D array isn't nullptr
 * @return nothing
 * @brief The current object is initialized with the content of the input Cmatrix of any type
 */
template<class type>
Cmatrix<type>::Cmatrix(unsigned int iNbColumnsPARAM, unsigned int iNbRowsPARAM, type** pTYParray2DPARAM)
{
	iNbColumns = iNbColumnsPARAM;
	iNbRows = iNbRowsPARAM;

	if (pTYParray2D != nullptr)
	{
		for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
		{
			delete[] pTYParray2D[iLoopRows];
		}
		delete[] pTYParray2D;
	}

	pTYParray2D = nullptr;

	if (iNbRows > 0 && iNbColumns > 0)
	{
		pTYParray2D = new type*[iNbRows];

		for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
		{
			pTYParray2D[iLoopRows] = new type[iNbColumns];
		}

		if (pTYParray2DPARAM != nullptr)
		{
			for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
			{
				for (unsigned int iLoopColumns = 0; iLoopColumns < iNbColumns; iLoopColumns++)
				{
					pTYParray2D[iLoopRows][iLoopColumns] = pTYParray2DPARAM[iLoopRows][iLoopColumns];
				}
			}
		}
	}
	else
	{
		pTYParray2D = nullptr;
	}
}


/**
 * @param[in] one const reference of a Cmatrix of any type
 * @pre nothing
 * @return nothing
 * @brief The current object is initialized with the content of the input Cmatrix of any type
 */
template<class type>
Cmatrix<type>::Cmatrix(const Cmatrix<type>& MATparam)
{
	iNbColumns = MATparam.iNbColumns;
	iNbRows = MATparam.iNbRows;

	if (iNbColumns > 0 && iNbRows > 0 && MATparam.pTYParray2D != nullptr)
	{
		pTYParray2D = new type*[iNbRows];
		for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
		{
			pTYParray2D[iLoopRows] = new type[iNbColumns];
		}

		for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
		{
			for (unsigned int iLoopColumns = 0; iLoopColumns < iNbColumns; iLoopColumns++)
			{
				pTYParray2D[iLoopRows][iLoopColumns] = MATparam.pTYParray2D[iLoopRows][iLoopColumns];
			}
		}
	}

}


/**
 * @param[in] A reference to file_matrix
 * @pre nothing
 * @return nothing
 * @brief The current object is initialized with the content of the file_matrix
 */
template<class type>
Cmatrix<type>::Cmatrix(Cfile_matrix& FIMparam)
{
	if (strcmp(FIMparam.FILget_file_type(), "double") != 0)
	{
		Cexception EXCobject;
		EXCobject.EXCset_value(MATexceptionTypeChosenAreNotCompatibleWithThisConstructor);
		throw(EXCobject);
	}
	if (FIMparam.FILget_file_nb_columns() < 0 || FIMparam.FILget_file_nb_rows() < 0)
	{
		Cexception EXCobject;
		EXCobject.EXCset_value(MATexceptionSpecifiedNumbersOfColumnsOrRowsAreNotCompatible);
		throw(EXCobject);
	}
	else
	{
		iNbColumns = FIMparam.FILget_file_nb_columns();
		iNbRows = FIMparam.FILget_file_nb_rows();
		
		if (iNbColumns > 0 && iNbRows > 0)
		{
			pTYParray2D = new type*[iNbRows];
			for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
			{
				pTYParray2D[iLoopRows] = new type[iNbColumns];
			}

			int a = 0;

			for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
			{
				for (unsigned int iLoopColumns = 0; iLoopColumns < iNbColumns; iLoopColumns++)
				{
					pTYParray2D[iLoopRows][iLoopColumns] = FIMparam.FILget_file_matrix_content()[a];
					a++;
				}
			}
		}

	}
}


/**
 * @param[in] nothing
 * @pre nothing
 * @return nothing
 * @brief The current object is deallocated
 */
template<class type>
Cmatrix<type>::~Cmatrix()
{
	if (pTYParray2D != nullptr)
	{
		for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
		{
			delete[] pTYParray2D[iLoopRows];
		}
		delete[] pTYParray2D;
		pTYParray2D = nullptr;
	}
}


/**
 * @param[in] nothing
 * @pre nothing
 * @return A reference of a Cmatrix of any type
 * @brief A new Cmatrix of any type is created and is the transposed matrix of the calling matrix
 */
template<class type>
Cmatrix<type> Cmatrix<type>::MATtranspose_matrix()
{
	Cmatrix MATres(iNbRows, iNbColumns, nullptr);

	for (unsigned int iLoopRows = 0; iLoopRows < MATres.iNbRows; iLoopRows++)
	{
		for (unsigned int iLoopColumns = 0; iLoopColumns < MATres.iNbColumns; iLoopColumns++)
		{
			MATres.pTYParray2D[iLoopRows][iLoopColumns] = pTYParray2D[iLoopColumns][iLoopRows];
		}
	}

	return MATres;
}

/**
 * @param[in] nothing
 * @pre nothing
 * @return A triangular matrix of any type
 * @brief A new Cmatrix of any type is created and is the triangular, factorized (with the Cholesky's method) of the calling matrix
 */
template<class type>
Cmatrix<type> Cmatrix<type>::MATfactorization_Cholesky()
{
	//We test if the current matrix is symmetrical (and also a square matrix) or not
	if (iNbRows == iNbColumns)
	{
		for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
		{
			for (unsigned int iLoopColumns = 0; iLoopColumns < iNbColumns; iLoopColumns++)
			{
				if (iLoopRows != iLoopColumns)
				{
					if (pTYParray2D[iLoopRows][iLoopColumns] != pTYParray2D[iLoopColumns][iLoopRows])
					{
						Cexception EXCobject;
						EXCobject.EXCset_value(MATexceptionMatrixNotSymmetrical);
						throw(EXCobject);
					}
				}
			}
		}

			Cmatrix<type> MATtriangular(iNbRows, iNbColumns, nullptr);

			for (unsigned int iLoopRows = 0; iLoopRows < MATtriangular.iNbRows; iLoopRows++)
			{
				for (unsigned int iLoopColumns = 0; iLoopColumns < MATtriangular.iNbColumns; iLoopColumns++)
				{
					MATtriangular.pTYParray2D[iLoopRows][iLoopColumns] = 0; //at the beginning of program the triangular matrix is full of zero
				}
			}

			float sumK; //the value of b[i][k]� for k = 1 -> i-1
			float resSum; //the value of a[i][i] - sumK
			int iKColumns;
			for(unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
			{
				for(unsigned int iLoopColumns = 0; iLoopColumns < iNbColumns; iLoopColumns++)
				{
					resSum = 0;
					sumK = 0;
					if(iLoopRows == iLoopColumns)
					{
						iKColumns = 0;
						while(iKColumns <= (int)(iLoopRows-1))
						{
							sumK += pow(MATtriangular.pTYParray2D[iLoopRows][iKColumns], 2);
							iKColumns++;
						}
						
						resSum = pTYParray2D[iLoopRows][iLoopRows] - sumK;
						if(resSum <= 0)
						{
							Cexception EXCobject;
							EXCobject.EXCset_value(MATexceptionFactorizationCholeskyMatrixNotPositiveDefinite);
							throw(EXCobject);
						}
						else
						{
							MATtriangular.pTYParray2D[iLoopRows][iLoopRows] = sqrt(resSum);
						}		
					}
					else
					{
						iKColumns = 0;
						while(iKColumns <= (int)(iLoopRows-1))
						{
							sumK += MATtriangular.pTYParray2D[iLoopRows][iKColumns] * MATtriangular.pTYParray2D[iLoopColumns][iKColumns];
							iKColumns++;
						}

						if ((pTYParray2D[iLoopRows][iLoopColumns] - sumK) == 0 || MATtriangular.pTYParray2D[iLoopRows][iLoopRows] == 0)
						{
							MATtriangular.pTYParray2D[iLoopColumns][iLoopRows] = 0; //we can't divide 0 by something or divide something by 0
						}
						else
						{
							MATtriangular.pTYParray2D[iLoopColumns][iLoopRows] = (pTYParray2D[iLoopRows][iLoopColumns] - sumK) / MATtriangular.pTYParray2D[iLoopRows][iLoopRows];
						}

					}
				}
			}

			return MATtriangular;
	}
	else
	{
		Cexception EXCobject;
		EXCobject.EXCset_value(MATexceptionMatrixNotSymmetrical);
		throw(EXCobject);
	}



}


/**
 * @param[in]  one const reference of a Cmatrix of any type
 * @pre nothing
 * @return the reference of the current Cmatrix
 * @brief The current object is modified : the number of rows and columns, and the values in the array are replaced by the number of rows and columns, and the values in the array of the parameter, which is a const Cmatrix of any type.
 */
template<class type>
Cmatrix<type>& Cmatrix<type>::operator=(const Cmatrix<type>& MATparam)
{
	if (this != &MATparam)
	{
		iNbRows = MATparam.iNbRows;
		iNbColumns = MATparam.iNbColumns;

		if (pTYParray2D != nullptr)
		{
			for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
			{
				delete[] pTYParray2D[iLoopRows];
			}
			delete[] pTYParray2D;
			pTYParray2D = nullptr;
		}

		if (MATparam.pTYParray2D != nullptr && pTYParray2D == nullptr)
		{
			if (iNbRows > 0 && iNbColumns > 0)
			{
				pTYParray2D = new type*[iNbRows];
			}

			for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
			{
				pTYParray2D[iLoopRows] = new type[iNbColumns];
			}

			for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
			{
				for (unsigned int iLoopColumns = 0; iLoopColumns < iNbColumns; iLoopColumns++)
				{
					pTYParray2D[iLoopRows][iLoopColumns] = MATparam.pTYParray2D[iLoopRows][iLoopColumns];
				}
			}
		}
			
	}

	return *this;
}


/**
 * @param[in] one const reference of a Cmatrix of any type
 * @pre nothing
 * @return the Cmatrix of the result of the addition
 * @brief Add the values of the current Cmatrix with the values of the Cmatrix in parameter.
 */
template<class type>
Cmatrix<type> Cmatrix<type>::operator+(const Cmatrix<type>& MATparam) //MATRIX A + MATRIX B => MATRIX A.operator+(MATRIX B)
{
	if ((iNbColumns != MATparam.iNbColumns) || (iNbRows != MATparam.iNbRows))
	{
		Cexception EXCobject;
		EXCobject.EXCset_value(MATexceptionDifferentDimensions);
		throw(EXCobject);
	}
	else
	{
		Cmatrix MATres(MATparam.iNbColumns, MATparam.iNbRows, nullptr);

		for (unsigned int iLoopRows = 0; iLoopRows < MATres.iNbRows; iLoopRows++)
		{
			for (unsigned int iLoopColumns = 0; iLoopColumns < MATres.iNbColumns; iLoopColumns++)
			{
				MATres.pTYParray2D[iLoopRows][iLoopColumns] = pTYParray2D[iLoopRows][iLoopColumns] + MATparam.pTYParray2D[iLoopRows][iLoopColumns];
			}
		}

		return MATres;
	}

}


/**
 * @param[in] one const reference of a Cmatrix of any type
 * @pre nothing
 * @return the Cmatrix of the result of the substraction
 * @brief Substract the values of the current Cmatrix by the value of the Cmatrix in parameter.
 */
template<class type>
Cmatrix<type> Cmatrix<type>::operator-(const Cmatrix<type>& MATparam) //MATRIX A - MATRIX B => MATRIX A.operator-(MATRIX B)
{
	if ((iNbColumns != MATparam.iNbColumns) || (iNbRows != MATparam.iNbRows)) //Impossible to make the calculus if the dimensions aren't the same
	{
		Cexception EXCobject;
		EXCobject.EXCset_value(MATexceptionDifferentDimensions);
		throw(EXCobject);
	}
	else
	{
		Cmatrix MATres(MATparam.iNbColumns, MATparam.iNbRows, nullptr);

		for (unsigned int iLoopRows = 0; iLoopRows < MATres.iNbRows; iLoopRows++)
		{
			for (unsigned int iLoopColumns = 0; iLoopColumns < MATres.iNbColumns; iLoopColumns++)
			{
				MATres.pTYParray2D[iLoopRows][iLoopColumns] = pTYParray2D[iLoopRows][iLoopColumns] - MATparam.pTYParray2D[iLoopRows][iLoopColumns];
			}
		}
		return MATres;
	}
}


/**
 * @param[in] one const reference of a Cmatrix of any type
 * @pre nothing
 * @return the Cmatrix of the result of the multiplication
 * @brief Multiplies the values of the current Cmatrix by the value of the Cmatrix in parameter.
 */
template <class type>
Cmatrix<type> Cmatrix<type>::operator*(const Cmatrix<type>& MATparam) //MATRIX A * MATRIX B => MATRIX A.operator*(MATRIX B)
{
	if (iNbColumns != MATparam.iNbRows)
	{
		Cexception EXCobject;
		EXCobject.EXCset_value(MATexceptionNumberOfColumnsOfMatrix1IsNotCompatibleWithNumberOfRowsOfMatrix2);
		throw(EXCobject);
	}
	else
	{
		Cmatrix MATres(MATparam.iNbColumns, MATparam.iNbRows, nullptr);
		for (unsigned int iLoopRows = 0; iLoopRows < MATres.iNbRows; iLoopRows++)
		{
			for (unsigned int iLoopColumns = 0; iLoopColumns < MATres.iNbColumns; iLoopColumns++)
			{
				MATres.pTYParray2D[iLoopRows][iLoopColumns] = 0;
			}
		}
		for (unsigned int iLoopRowsM1 = 0; iLoopRowsM1 < iNbRows; iLoopRowsM1++)
		{
			for (unsigned int iLoopColumnsM2 = 0; iLoopColumnsM2 < MATparam.iNbColumns; iLoopColumnsM2++)
			{
				for (unsigned int iLoopRowsM2 = 0; iLoopRowsM2 < MATparam.iNbRows; iLoopRowsM2++)
				{
					MATres.pTYParray2D[iLoopRowsM1][iLoopColumnsM2] += pTYParray2D[iLoopRowsM1][iLoopRowsM2] * MATparam.pTYParray2D[iLoopRowsM2][iLoopColumnsM2];
				}
			}
		}
		return MATres;
	}

}


/**
 * @param[in] nothing
 * @pre nothing
 * @return The assigned value of the data mumber pTYParray2D of the current object
 * @brief nothing
 */
template<class type>
inline type** Cmatrix<type>::MATget_array2D()
{
	return pTYParray2D;
}


/**
 * @param[in] nothing
 * @pre nothing
 * @return The assigned value of the data mumber iNbColumns of the current object
 * @brief nothing
 */
template<class type>
inline unsigned int Cmatrix<type>::MATget_nb_columns()
{
	return iNbColumns;
}


/**
 * @param[in] nothing
 * @pre nothing
 * @return The assigned value of the data mumber iNbRows of the current object
 * @brief nothing
 */
template<class type>
inline unsigned int Cmatrix<type>::MATget_nb_rows()
{
	return iNbRows;
}


/**
 * @param[in] one unsigned integer value corresponding to the numbers of rows, one unsigned integer value corresponding to the numbers of columns
 * @pre nothing
 * @return nothing
 * @brief We assigned a value to data mumber iEXCvalue of the current object
 */
template<class type>
void Cmatrix<type>::MATset_array2D(unsigned int iNbColumsPARAM, unsigned int iNbRowsPARAM)
{
	if (pTYParray2D != nullptr)
	{
		for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
		{
			delete[] pTYParray2D[iLoopRows];
		}
		delete[] pTYParray2D;
		pTYParray2D = nullptr;
	}

	iNbColumns = iNbColumsPARAM;
	iNbRows = iNbRowsPARAM;

	if (iNbRows > 0 && iNbColumns> 0)
	{
		pTYParray2D = new type*[iNbRows];
	}

	for (unsigned int iLoopRows = 0; iLoopRows < iNbRows; iLoopRows++)
	{
		pTYParray2D[iLoopRows] = new type[iNbColumns];
	}
}


/**
 * @param[in] the number of columns
 * @pre nothing
 * @return nothing
 * @brief sets the number of columns of the array of the Cmatrix by the value in parameter.
 */
template<class type>
inline void Cmatrix<type>::MATset_nb_columns(unsigned int iNbColumnsPARAM)
{
	iNbColumns = iNbColumnsPARAM;
}


/**
 * @param[in] the number of rows
 * @pre nothing
 * @return nothing
 * @brief sets the number of rows of the array of the Cmatrix by the value in parameter.
 */
template<class type>
inline void Cmatrix<type>::MATset_nb_rows(unsigned int iNbRowsPARAM)
{
	iNbRows = iNbRowsPARAM;
}
