To test the Factorization of Cholesky on matrices, everything is done in the main program (V2), you just need to execute the PROJECT_MATRIX.exe
/!\ YOU NEED TO EXECUTE IT WITH THE COMMAND PROMPT /!\

If you add matrices in arguments in the command prompt, the V1 program will be executed on those matrices but the V2 program (with the factorization)
will ignore them.